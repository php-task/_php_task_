<?php

        $short_help = "
                    Usage:
                    php user_upload.php --file csv_file_name [--create_table] [--dry_run] -u mysql_user_name -p mysql_password -h mysql_host [--help]

                    eg:
                    php user_upload.php --file users.csv -u root -p \"\" -h localhost --db users --dry_run
                    eg: creating table
                    php uu1.php -u root -p root -h localhost --db users1 --create_table
                    eg: dry run
                    php uu1.php -u root -p root -h localhost --db users1 --dry_run 
                    Use double quote for empty argument or argument is space. 
                    eg: Password is empty => -p \"\" ";

        $info = "$short_help what would you like to do?" . "\n" . "--file user.csv for data to be parsed and insert in to DB" . "\n" . "--create_table to create database table" . "\n" . "--dry_run for data to be parsed " . "\n" . "-u mysql username" . "\n" . "-p MySQL password" . "\n" . "-h  MySQL host" . "\n" . "--db  MySQL Database" . "\n" . "--help  for all menue." . "\n";
        
        $short_options = "";
        $short_options .= "u:"; // Required value, -u Mysql username
        $short_options .= "p:"; // Required value, -p Mysql password
        $short_options .= "h:"; // Required value, -h Mysql host

        $long_options = array(
            "db:", // Required value, --file
            "file:", // Required value, --file
            "create_table::", // Optional value, --create_table
            "dry_run::", // Optional value, --dry_run
            "help::", // Optional value, --help
            "option", // No value
            "opt" // No value
        );

        $options = getopt($short_options, $long_options);
        $arg_is_help = false;
        $check_option           = "help";

        if (array_key_exists($check_option, $options)) {
            $arg_is_help = true;
        }

        if ($arg_is_help) {
            echo $info;
            die();
        }

        $argstr_umysql = "";
        $check_option            = "u";

        if (array_key_exists($check_option, $options)) {
            $argstr_umysql = $options[$check_option];
        } 

        else {

                if (!$arg_is_dry_run) {
                die("-u mysql username required $short_help");
            }
        }

        $argstr_pmysql = "";
        $check_option            = "p";

        if (array_key_exists($check_option, $options)) {
            $argstr_pmysql = $options[$check_option];
        } 
        else {

            if (!$arg_is_dry_run) {
                die("-p mysql password required $short_help");
            }
            
        }

        $argstr_hmysql = "";
        $check_option             = "h";

        if (array_key_exists($check_option, $options)) {
            $argstr_hmysql = $options[$check_option];
        } 
        else {

            if (!$arg_is_dry_run) {
                die("-h mysql host required $short_help");
            }
        }

        $argstr_hmysql = "";
        $check_option             = "h";

        if (array_key_exists($check_option, $options)) {
            $argstr_hmysql = $options[$check_option];
        } 
        else {
            die("-h mysql host required $short_help");
        }


        $argstr_db = "";
        $check_option         = "db";

        if (array_key_exists($check_option, $options)) {
            $argstr_db = $options[$check_option];
        } 
        else {
            die("--db mysql database required $short_help");
        }

        $arg_is_create_table = false;
        $check_option  = "create_table";

        if (array_key_exists($check_option, $options)) {
            $arg_is_create_table = true;
        }

        $servername = "localhost";
        $username   = "root";
        $password   = "";
        $table      = "users";

        $servername = $argstr_hmysql;
        $username   = $argstr_umysql;
        $password   = $argstr_pmysql;
        $database   = $argstr_db;
        $table      = "users";


        if ($arg_is_create_table) {
   
        $conn = new mysqli($servername, $username, $password);
        // Create database
        $sql  = "CREATE DATABASE IF NOT EXISTS " . $argstr_db;

        if ($conn->query($sql) === TRUE) {
            echo "Database  success \n";
        } else {
            echo "Error creating database: " . $conn->error;
        }
    
        $conn = new mysqli($servername, $username, $password, $argstr_db);
        // Check connection
       
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
    
        $create_table = "CREATE TABLE  IF NOT EXISTS users (
            `name` varchar(500),
            `surname` varchar(500),
            `email` varchar(500) UNIQUE) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_unicode_ci
            ";
    
    
    
        if ($conn->query($create_table) === TRUE) {
            echo "Table created successfully \n";
        } else {
            echo " Error creating table: " . $conn->error;
        }
    
        $conn->close();
        die("Table created. Exiting... \n");
        
        }

        $arg_is_dry_run = false;
        $check_option             = "dry_run";

        if (array_key_exists($check_option, $options)) {
            $arg_is_dry_run = true;
        }


        $argstr_csv_file_loc = "";
        $check_option  = "file";

        if (array_key_exists($check_option, $options)) {
            $argstr_csv_file_loc = $options[$check_option];
        } else {
            die("--file csv_file_name required $short_help");
        }

        if ($arg_is_help) {
            echo $info;
        }

        if (!$arg_is_dry_run) {
            $conn = new mysqli($servername, $username, $password, $argstr_db);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
        }

        $csv_file = "./users.csv"; // Name of your CSV file intialise 
        $csv_file = $argstr_csv_file_loc; //csv file from input

        if (file_exists($csv_file)) {
            //echo "The file $filename exists";
        } else {
            die("$csv_file not found");
        }
        $csvfile = fopen($csv_file, 'r');
        $theData = fgets($csvfile); //skip header  
        $counter       = 0;
        while (!feof($csvfile)) {
    
        $csv_data[] = fgets($csvfile, 1000000);
        $csv_array  = explode(",", $csv_data[$counter]);
        $counter++;
        
        // print_r($csv_array);
        $count = count($csv_array);

        if ($count < 3) {
            continue;
        }
    
        
        $name    = ucfirst(strtolower($csv_array[0]));
        $surname = ucfirst(strtolower($csv_array[1]));
        $email   = strtolower(trim($csv_array[2]));
        $email   = filter_var($email, FILTER_VALIDATE_EMAIL);


        $str_csv_row = "'$name', '$surname', '$email'";

        if ($arg_is_dry_run) {
            echo "$str_csv_row \n";
        } else {
        $query = "INSERT INTO users(name,surname,email) VALUES(?, ?, ?)";
        $stmt  = $conn->prepare($query);
        $stmt->bind_param("sss", $name, $surnme, $email); //avoiding sql injection, escape quote

        if ($stmt->execute() === TRUE) {
            echo "$str_csv_row Inserted successfully";
        } else {
            echo " Error creating record: " . $conn->error . "\n";
                 }
        }

        }          
        fclose($csvfile);

        if (!$arg_is_dry_run) {
            $conn->close();
        }




?>